#include <iostream>
#include <ros/ros.h>
#include <ros/console.h>
#include <image_transport/image_transport.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include "std_msgs/String.h"



int main(int argc, char **argv){
	

	ros::init(argc, argv, "streamer");
	ros::NodeHandle n;
	image_transport::ImageTransport it_(n);
	image_transport::Publisher image_pub_;
	image_pub_ = it_.advertise("/camera/image_raw", 1);
	
	cv::Mat img;
	cv::VideoCapture cap(0);
	cv_bridge::CvImage img_bridge;
	sensor_msgs::Image img_msg;
	
	std_msgs::Header header;
	
	if(!cap.isOpened()){
		ROS_ERROR("Could not open the camera.\n");
	}
	ROS_INFO("Camera is open.\n");
	
	ros::Rate loop_rate(60);
	int count = 0;


	while(ros::ok()){
		cap >> img;
		header.seq = count; 
		header.stamp = ros::Time::now(); 
		img_bridge = cv_bridge::CvImage(header, sensor_msgs::image_encodings::RGB8, img);
		img_bridge.toImageMsg(img_msg);
		image_pub_.publish(img_msg);
		count++;
		loop_rate.sleep();
	}

	return 0;
}
